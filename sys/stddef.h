#pragma once

#define __i386__ 1

#include <gcc-stddef.h>
#include <sys/config.h>

#define EOF (-1)

typedef _Bool bool_t;

#define FALSE (0)
#define TRUE (1)