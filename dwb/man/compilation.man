.TH COMPILATION 1 "28 Nov 2021"
.SH COMPILATION
Compilation process \- This document explains how to build the kernel
.SH SYNOPSIS
\fBsh autogen.sh && make\fP [ default | qemu ]
.SH DESCRIPTION
Building the kernel is made easy for the end user using the GNU autoconf system.
To configure the build, simply run \fIsh autogen\fP.
This assumes \fBcompiledb\fP to be installed.
It creates a compile_commands.json file, which is used by many langauge servers.
To actually compile, run \fImake\fP.
.SS Options
.TP
\fBmake\fP
Regular build
.TP
\fBmake default\fP
Same as \fBmake\fP
.TP
\fBmake qemu\fP
Debug with qemu.
Qemu will use any built kernel in the root directory.
.SH "SEE ALSO"
autoconf(1), make(1), sh(1)