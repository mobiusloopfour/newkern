.TH GENERAL 1 "28 Nov 2021"
.SH GENERAL
General info \- This document gives an overview of the project
.SH DESCRIPTION
NewKern is the kernel for a yet to be implemented multi-platform pre-emptive multitasking operating system.
Unlike most hobby OS projects, it does not aim to be POSIX-compliant, 
and does not try to be binary compatible with exising operating systems.
.SS Currently under development:
.TP
\fBCPU initialization\fP
.TP
\fBA standard library\fP
.TP
\fBMultitasking\fP
.SH "SEE ALSO"
wiki.osdev.org